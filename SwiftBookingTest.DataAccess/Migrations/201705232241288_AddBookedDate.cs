namespace SwiftBookingTest.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBookedDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Client", "BookedDate", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Client", "BookedDate");
        }
    }
}
