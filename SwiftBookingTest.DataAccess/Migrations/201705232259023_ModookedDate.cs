namespace SwiftBookingTest.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModookedDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Client", "BookedDate", c => c.DateTime(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Client", "BookedDate", c => c.String());
        }
    }
}
