﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using SwiftBookingTest.Models;

namespace SwiftBookingTest.DataAccess
{
    public class BookingTestContext : DbContext
    {
      
        public BookingTestContext() : base("DefaultConnection")
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
