﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SwiftBookingTest.DataAccess;
using SwiftBookingTest.Models;
using SwiftBookingTest.Web.APIControllers;
using SwiftBookingTest.Web.Utilities;

namespace SwiftBookingTest.Tests
{
	[TestClass]
	public class ClientControllerTests
	{
		[TestMethod]
		public async Task IndexTest()
		{
			var data = new List<Client>
			{
				new Client{ Name = "c1", InsertDate = DateTime.Parse("2017-04-01 12:00:00")},
				new Client{ Name = "c5", InsertDate = DateTime.Parse("2016-12-01 12:00:00")},
				new Client{ Name = "c3", InsertDate = DateTime.Parse("2017-02-01 12:00:00")},
				new Client{ Name = "c2", InsertDate = DateTime.Parse("2017-03-01 12:00:00")},
				new Client{ Name = "c4", InsertDate = DateTime.Parse("2017-01-01 12:00:00")}
			};

			// Create a mock set and context
			var set = new Mock<DbSet<Client>>()
				.SetupData(data);

			var apiClient = new Mock<ISwiftApi>();

			var context = new Mock<BookingTestContext>();
			context.Setup(c => c.Clients).Returns(set.Object);

			// Create a BlogsController and invoke the Index action
			var controller = new ClientController(context.Object);
			var result = await controller.Get(2, 3);



			Assert.AreEqual(3, result.Count);
			Assert.AreEqual("c3", result[0].Name);
			Assert.AreEqual("c4", result[1].Name);
			Assert.AreEqual("c5", result[2].Name);
		}
	}
}
