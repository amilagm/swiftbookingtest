﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SwiftBookingTest.DataAccess;
using SwiftBookingTest.Models;
using SwiftBookingTest.Web.APIControllers;
using SwiftBookingTest.Web.Utilities;

namespace SwiftBookingTest.Tests
{
	[TestClass]
	public class DeliveryControllerTests
	{
		[TestMethod]
		public async Task IndexTest()
		{

			var Id = Guid.NewGuid();


			var data = new List<Client>
			{
				new Client{ Name = "c1", InsertDate = DateTime.Parse("2017-04-01 12:00:00"),Id = Guid.NewGuid()},
				new Client{ Name = "c5", InsertDate = DateTime.Parse("2016-12-01 12:00:00"),Id = Guid.NewGuid()},
				new Client{ Name = "c3", InsertDate = DateTime.Parse("2017-02-01 12:00:00"),Id = Guid.NewGuid()},
				new Client{ Name = "c2", InsertDate = DateTime.Parse("2017-03-01 12:00:00"),Id = Guid.NewGuid()},
				new Client{ Name = "c4", InsertDate = DateTime.Parse("2017-01-01 12:00:00"),Id = Id }
			};

			// Create a mock set and context
			var set = new Mock<DbSet<Client>>()
				.SetupData(data);

			var apiClient = new Mock<ISwiftApi>();
			apiClient.Setup(c => c.AddDeliveryAsync(It.IsAny<Client>())).ReturnsAsync("uploaded");

			var context = new Mock<BookingTestContext>();
			context.Setup(c => c.Clients).Returns(set.Object);
			context.Setup(c => c.SaveChangesAsync()).ReturnsAsync(1);

			// Create a BlogsController and invoke the Index action
			var controller = new BookingController(context.Object, apiClient.Object);
			var result = await controller.SwiftDelivery(Id);


			Assert.AreEqual("uploaded", result.apiResponse);
			Assert.AreEqual("c4", result.client.Name);
			Assert.AreEqual(DateTime.UtcNow.Date, result.client.BookedDate.Date);

		}
	}
}
