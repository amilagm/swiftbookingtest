﻿using System;

namespace SwiftBookingTest.Models
{
    public class Client
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime InsertDate{ get; set; }
        public string Address { get; set; }
        public DateTime? BookedDate { get; set; }
    }
}
