﻿using SwiftBookingTest.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SwiftBookingTest.Web.Utilities
{
    public class SwiftApi : ISwiftApi
    {
        private HttpClient _httpClient;
        private const string ApiKey = "3285db46-93d9-4c10-a708-c2795ae7872d";

        public SwiftApi(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Adds a delivery via the swift API
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<string> AddDeliveryAsync(Client client)
        {
            var request = new
            {
                apiKey = ApiKey,
                booking = new
                {
                    pickupDetail = new
                    {
                        name = "Rupert",
                        phone = "1234567890",
                        address = "57 luscombe st, brunswick, melbourne"
                    },
                    dropoffDetail = new
                    {
                        name = client.Name,
                        phone = client.PhoneNumber,
                        address = client.Address
                    }
                }
            };

            var apiResponse = await _httpClient.PostAsJsonAsync("https://app.getswift.co/api/v2/deliveries", request);

            if(!apiResponse.IsSuccessStatusCode)
                throw  new Exception("Call to the API failed "  + apiResponse.ReasonPhrase);
            //TODO log the response content to the log

            var apiResponseText = await apiResponse.Content.ReadAsStringAsync();

            return apiResponseText;
        }
    }
}