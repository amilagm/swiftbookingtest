﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using Ninject.Modules;
using SwiftBookingTest.DataAccess;

namespace SwiftBookingTest.Web.Utilities
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<ISwiftApi>().To<SwiftApi>();

            //HttPClient is heavy and meant to be singleton in server apps
            Bind<HttpClient>().To<HttpClient>().InSingletonScope();
        }
    }
}