﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace SwiftBookingTest.Web.Utilities
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ErrorHandleAttribute:ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;

            if(modelState.IsValid)
                return;

            var modelErrors = modelState.Where(d => d.Value.Errors != null && d.Value.Errors.Any())
                .Select(p => new
                {
                    fieldName = string.Join(".", p.Key.Split('.').Skip(1).ToArray()),
                    error = string.Join(",", p.Value.Errors.Select(e => e.ErrorMessage))
                })
                .ToList(); //remove the first part of the name (name looks like client.Address)

            var response = new
            {
                errors = modelErrors
            };

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, response);
        }
    }
}