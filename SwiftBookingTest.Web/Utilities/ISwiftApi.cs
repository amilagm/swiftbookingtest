﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SwiftBookingTest.Models;

namespace SwiftBookingTest.Web.Utilities
{
    public interface ISwiftApi
    {
        Task<string> AddDeliveryAsync(Client client);
    }
}