﻿using System;
using System.Collections.Generic;
using SwiftBookingTest.Models;
using SwiftBookingTest.Web.Models;
using WebGrease.Css.Extensions;

namespace SwiftBookingTest.Web.MappingExtensions
{
    public static class ClientMapper
    {
        /// <summary>
        /// Converts a list of entities models to viewModels
        /// </summary>
        /// <param name="clients">List of entities</param>
        /// <returns></returns>
        public static IList<ClientViewModel> ToViewModels(IList<Client> clients)
        {
            var viewModels = new List<ClientViewModel>();

            clients.ForEach(c => viewModels.Add( new ClientViewModel
            {
                InsertDate = c.InsertDate,
                Name = c.Name,
                PhoneNumber = c.PhoneNumber,
                Id = c.Id,
                Address = c.Address,
                BookedDate = c.BookedDate
            } ));

            return viewModels;
        }

        /// <summary>
        /// Converts a viewModel to an entity for inserting. This method sets the Id to a new Guid and the InsertDate to the current UTC time
        /// </summary>
        /// <param name="clientViewModel"></param>
        /// <returns>A Client enity to be insterted for the db</returns>
        public static Client ToEntityForInserting(ClientViewModel clientViewModel)
        {
            return new Client
            {
                Name = clientViewModel.Name,
                PhoneNumber = clientViewModel.PhoneNumber,
                Id = Guid.NewGuid(),
                InsertDate = DateTime.UtcNow,
                Address = clientViewModel.Address,
                BookedDate = clientViewModel.BookedDate
                

            };
        }

        /// <summary>
        /// Converts a given Clent entity to a client viewModel
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static ClientViewModel ToViewModel(Client entity)
        {
            return new ClientViewModel
            {
                Id = entity.Id,
                InsertDate = entity.InsertDate,
                PhoneNumber = entity.PhoneNumber,
                Name = entity.Name,
                Address = entity.Address,
                BookedDate = entity.BookedDate
                
            };
        }
    }
}