﻿var pageViewModel = function(){
    var self = this;

    self.inProgress = ko.observable(true);
    self.clients = ko.observableArray();
    self.loadingFailure = ko.observable(false);
    self.saveInProgress = ko.observable(false);
    self.saveFailure = ko.observable(false);

    $.getJSON("/api/client",
        function(data) {
            data.forEach(function(elem) {
                var addedModel = new ClientViewModel(elem.Id, elem.Address, elem.InsertDate, elem.BookedDate);
                self.clients.push(addedModel);

            });
        }).fail(function() {
        self.loadingFailure(true);
    }).always(function() {
        self.inProgress(false);
    });


    self.callApi = function(data) {

        data.inProgress(true);
       

        $.ajax({
            url: "/api/booking",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: '"' + data.id() + '"',
                success: function (res) {
                    var updateModel = new ClientViewModel(res.client.Id, res.client.Address, res.client.InsertDate, res.client.BookedDate);
                    self.clients().forEach(function(item) {
                        if (item.id() === updateModel.id()) {
                            item.bookedDate(updateModel.bookedDate());
                            $('#apiResult').text(res.apiResponse);
                        }
                    });
                }
            }
        ).fail(function (response) {
            $('#apiResult').text("Call Failed");

        }).always(function () {
            data.inProgress(false);
        });
    };


    self.saveData = function () {

        $('#bookingForm').data('formValidation').validate();
        if (!$('#bookingForm').data('formValidation').isValid())
            return;

        var clientData = {
            Name: $("#txtName").val(),
            PhoneNumber: $('#txtPhone').val(),
            Address: $('#txtAddress').val()
        };

        self.saveInProgress(true);
        self.saveFailure(false);

        $('#bookingForm').data('formValidation').resetForm();

        $.ajax({
                url: "/api/client",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(clientData),
                success: function(data) {
                    var addedModel = new ClientViewModel(data.Id, data.Address,data.insertDate, data.BookedDate);
                    self.clients.unshift(addedModel);

                    $("#txtName").val('');
                    $('#txtPhone').val('');
                    $('#txtAddress').val('');
                }
            }
        ).fail(function(response) {
            if (response.status === 400) {

                //show  the message from the server
                var formVal = $('#bookingForm').data('formValidation');

                response.responseJSON.errors.forEach(function (item) {
                    formVal.updateMessage(item.fieldName, 'blank', item.error);
                    formVal.updateStatus(item.fieldName, 'INVALID', 'blank');
               
                });
            }

        }).always(function() {
            self.saveInProgress(false);
        });
    };
};

var ClientViewModel = function (id, address,insertDate, bookedDate) {

    var self = this;
    self.id =ko.observable(id);
    self.address = ko.observable(address);
    self.insertDate = ko.observable(insertDate);
    self.bookedDate = ko.observable(bookedDate);
    self.inProgress = ko.observable(false);

};

ko.applyBindings(pageViewModel);





//validations
$(document).ready(function() {  
      
    $('#bookingForm').formValidation({
        // I am validating Bootstrap form
        framework: 'bootstrap',

        // Feedback icons
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        }
        
    });

    //custom validator to show server messages
 

});