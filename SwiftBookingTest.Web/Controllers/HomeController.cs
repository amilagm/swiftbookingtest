﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using SwiftBookingTest.DataAccess;
using SwiftBookingTest.Web.MappingExtensions;

namespace SwiftBookingTest.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly BookingTestContext _context;

        public HomeController(BookingTestContext bookingTestContext)
        {
            _context = bookingTestContext;
        }


        //
        // GET: /Home/
        public  ActionResult Index()
        {
            return View();
        }

    }
}
