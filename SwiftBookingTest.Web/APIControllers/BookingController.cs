﻿using SwiftBookingTest.DataAccess;
using SwiftBookingTest.Web.MappingExtensions;
using SwiftBookingTest.Web.Models;
using SwiftBookingTest.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SwiftBookingTest.Web.APIControllers
{
    public class BookingController : ApiController
    {
        private readonly BookingTestContext _context;
        private readonly ISwiftApi _swiftApi;

        public BookingController(BookingTestContext context, ISwiftApi swiftApi)
        {
            _context = context;
            _swiftApi = swiftApi;
        }

        [HttpPost]
        public async Task<dynamic> SwiftDelivery([FromBody]Guid clientId)
        {
            var clientEntity = await _context.Clients.FirstOrDefaultAsync(c => c.Id == clientId);

            if (clientEntity == null)
                throw new HttpException(404, "Client with the given Id cannot be found");

            try
            {
                var response = await _swiftApi.AddDeliveryAsync(clientEntity);
                clientEntity.BookedDate = DateTime.UtcNow;

                await _context.SaveChangesAsync();

                return new
                {
                    apiResponse = response,
                    client = ClientMapper.ToViewModel(clientEntity)
                };

            }
            catch (Exception e)
            {
                throw new HttpException(500, "An Error occured calling the Swift API");
                //TODO log e
            }

        }
    }
}
