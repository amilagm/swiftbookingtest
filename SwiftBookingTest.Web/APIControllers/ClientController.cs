﻿using SwiftBookingTest.DataAccess;
using SwiftBookingTest.Web.MappingExtensions;
using SwiftBookingTest.Web.Models;
using SwiftBookingTest.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace SwiftBookingTest.Web.APIControllers
{
    public class ClientController : ApiController
    {
        private readonly BookingTestContext _context;
      

        public ClientController(BookingTestContext context)
        {
            _context = context;
        }

        [ErrorHandle]
        public async Task<ClientViewModel> Post(ClientViewModel client)
        {
            var entity = ClientMapper.ToEntityForInserting(client);

            _context.Clients.Add(entity);

            await _context.SaveChangesAsync();

            await _context.Entry(entity).ReloadAsync();

            return ClientMapper.ToViewModel(entity);
        }

        public async Task<IList<ClientViewModel>> Get(int skip = 0, int take = 10)
        {
            var clients = await _context.Clients.OrderByDescending(c => c.InsertDate).Skip(skip).Take(take).ToListAsync();
            return ClientMapper.ToViewModels(clients);
        }

      
    }
}
